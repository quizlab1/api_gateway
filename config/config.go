package config

import (
	"os"

	"github.com/spf13/cast"
)

var (
	// FollowType enum
	FollowType = "follow"
	// UnfollowType enum
	UnfollowType = "unfollow"
	// LikeType enum
	LikeType = "like"
)

const (
	// AndroidMinVersion is needed if force update is integrated
	AndroidMinVersion string = "0.0.0"
	// IOSMinVersion is needed if force update is integrated
	IOSMinVersion string = "0.0.0"
)

// Config ...
type Config struct {
	Environment string // develop, staging, production

	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string

	RedisHost string
	RedisPort int

	UserServiceHost string
	UserServicePort int

	NotifServiceHost string
	NotifServicePort int

	BookServiceHost string
	BookServicePort int

	CasbinConfigPath string

	// context timeout in seconds
	CtxTimeout int

	SigninKey string
	LogLevel  string
	HTTPPort  string
	GinMode   string

	FirebaseWebKey     string
	DomainURIPrefix    string
	AndroidPackageName string
	IosBundleID        string

	GoogleAPIKey string
}

// Load loads environment vars and inflates Config
func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))
	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(getOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DB", "quizlabdb"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "postgres"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "1234"))

	c.RedisHost = cast.ToString(getOrReturnDefault("REDIS_HOST", "localhost"))
	c.RedisPort = cast.ToInt(getOrReturnDefault("REDIS_PORT", 6379))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8000"))

	c.UserServiceHost = cast.ToString(getOrReturnDefault("USER_SERVICE_HOST", "localhost"))
	c.UserServicePort = cast.ToInt(getOrReturnDefault("USER_SERVICE_PORT", 9000))
	c.NotifServiceHost = cast.ToString(getOrReturnDefault("NOTIFICATION_SERVICE_HOST", "localhost"))
	c.NotifServicePort = cast.ToInt(getOrReturnDefault("NOTIFICATION_SERVICE_PORT", 9001))
	c.BookServiceHost = cast.ToString(getOrReturnDefault("BOOK_SERVICE_HOST", "localhost"))
	c.BookServicePort = cast.ToInt(getOrReturnDefault("BOOK_SERVICE_PORT", 9002))

	c.CasbinConfigPath = cast.ToString(getOrReturnDefault("CASBIN_CONFIG_PATH", "./config/rbac_model.conf"))
	c.SigninKey = cast.ToString(getOrReturnDefault("SIGNING_KEY", "BsHZrCEQqhq0AHochcubd97hqDbZ5Wy8ypMcOEyNfRqpJLzCUc"))
	c.GinMode = cast.ToString(getOrReturnDefault("GIN_MODE", "debug"))

	c.CtxTimeout = cast.ToInt(getOrReturnDefault("CTX_TIMEOUT", 7))

	c.GoogleAPIKey = cast.ToString(getOrReturnDefault("GOOGLE_API_KEY", "AIzaSyDPpW8OFsv0abudEztivZ7dvghUxltny-4"))

	c.FirebaseWebKey = cast.ToString(getOrReturnDefault("FIREBASE_WEB_KEY", "AIzaSyDVp4xRIvEJFCPHAkw2P8_ocSR6jG2DeeU"))
	c.AndroidPackageName = cast.ToString(getOrReturnDefault("ANDROID_PACKAGE_NAME", "uz.wiut.quizlab"))
	c.IosBundleID = cast.ToString(getOrReturnDefault("IOS_BUNDLE_ID", "uz.wiut.quizlab"))
	c.DomainURIPrefix = cast.ToString(getOrReturnDefault("DOMAIN_URI_PREFIX", "https://quizlab.page.link"))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}
