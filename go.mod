module gitlab.com/quizlab/api_gateway

go 1.15

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/casbin/casbin/v2 v2.19.8
	github.com/casbin/gorm-adapter/v2 v2.1.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-ozzo/ozzo-validation/v3 v3.8.1
	github.com/golang/protobuf v1.5.0
	github.com/gomodule/redigo v1.8.3
	github.com/google/uuid v1.2.0
	github.com/hashicorp/go-version v1.2.1
	github.com/lib/pq v1.9.0
	github.com/spf13/cast v1.3.1
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.3.0
	github.com/swaggo/swag v1.5.1
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	google.golang.org/grpc v1.34.0
	google.golang.org/protobuf v1.26.0
)
