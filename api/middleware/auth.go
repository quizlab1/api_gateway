package middleware

import (
	"log"
	"net/http"
	"strings"

	"github.com/casbin/casbin/v2"
	jwtg "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/hashicorp/go-version"

	v1 "gitlab.com/quizlab/api_gateway/api/handlers/v1"
	"gitlab.com/quizlab/api_gateway/api/models"
	"gitlab.com/quizlab/api_gateway/api/token"
	"gitlab.com/quizlab/api_gateway/config"
)

//JWTRoleAuthorizer ...
type JWTRoleAuthorizer struct {
	enforcer   *casbin.Enforcer
	cfg        config.Config
	jwtHandler token.JWTHandler
}

// NewAuthorizer is a middleware for gin to get role and
// allow or deny access to endpoints
func NewAuthorizer(e *casbin.Enforcer, jwtHandler token.JWTHandler, cfg config.Config) gin.HandlerFunc {
	a := &JWTRoleAuthorizer{
		enforcer:   e,
		cfg:        cfg,
		jwtHandler: jwtHandler,
	}

	return func(c *gin.Context) {

		VersionMiddleware(c)

		allow, err := a.CheckPermission(c.Request)
		if err != nil {
			v, _ := err.(*jwtg.ValidationError)
			if v.Errors == jwtg.ValidationErrorExpired {
				a.RequireRefresh(c)
			} else {
				a.RequirePermission(c)
			}
		} else if !allow {
			a.RequirePermission(c)
		}
	}
}

// GetRole gets role from Authorization header if there is a token then it is
// parsed and in role got from role claim. If there is no token then role is
// unauthorized
func (a *JWTRoleAuthorizer) GetRole(r *http.Request) (string, error) {
	var (
		role   string
		claims jwtg.MapClaims
		err    error
	)

	jwtToken := r.Header.Get("Authorization")
	if jwtToken == "" {
		return "unauthorized", nil
	} else if strings.Contains(jwtToken, "Basic") {
		return "unauthorized", nil
	}

	a.jwtHandler.Token = jwtToken
	claims, err = a.jwtHandler.ExtractClaims()
	if err != nil {
		return "", err
	}

	if claims["role"].(string) == "authorized" {
		role = "authorized"
	} else {
		role = "unknown"
	}
	return role, nil
}

// CheckPermission checks whether user is allowed to use certain endpoint
func (a *JWTRoleAuthorizer) CheckPermission(r *http.Request) (bool, error) {
	user, err := a.GetRole(r)
	if err != nil {
		return false, err
	}
	method := r.Method
	path := r.URL.Path

	allowed, err := a.enforcer.Enforce(user, path, method)
	if err != nil {
		panic(err)
	}

	return allowed, nil
}

// RequirePermission aborts request with 403 status
func (a *JWTRoleAuthorizer) RequirePermission(c *gin.Context) {
	c.AbortWithStatus(403)
}

// RequireRefresh aborts request with 401 status
func (a *JWTRoleAuthorizer) RequireRefresh(c *gin.Context) {
	c.JSON(http.StatusUnauthorized, models.ResponseError{
		Error: models.ServerError{
			Status:  "UNAUTHORIZED",
			Message: "Token is expired",
		},
	})
	c.AbortWithStatus(401)
}

// WebsocketMiddleware is a middleware for gin to get role and
// allow or deny access to endpointns
func WebsocketMiddleware(e *casbin.Enforcer) gin.HandlerFunc {
	var (
		claims jwtg.MapClaims
		err    error
	)
	a := &JWTRoleAuthorizer{enforcer: e}

	return func(c *gin.Context) {
		a.jwtHandler.Token = c.Query("token")

		claims, err = a.jwtHandler.ExtractClaims()
		if err != nil {
			v, _ := err.(jwtg.ValidationError)
			if v.Errors == jwtg.ValidationErrorExpired {
				a.RequireRefresh(c)
				return
			}
			if claims["role"] == nil {
				c.AbortWithStatus(http.StatusUnauthorized)
				return
			}

			log.Println(err)
			c.AbortWithStatus(http.StatusBadRequest)
			return

		}
	}
}

// VersionMiddleware checks for version upgrades
func VersionMiddleware(c *gin.Context) {

	appVersion := c.GetHeader("Version")
	appPlatform := c.GetHeader("Platform")

	appV, err := version.NewVersion(appVersion)
	if err != nil {
		return
	}
	androidMinVersion, err := version.NewVersion(config.AndroidMinVersion)
	if err != nil {
		return
	}

	iosMinVersion, err := version.NewVersion(config.IOSMinVersion)
	if err != nil {
		return
	}

	if appPlatform == "android" {
		if appV.LessThan(androidMinVersion) {
			c.AbortWithStatusJSON(http.StatusUpgradeRequired, models.ServerError{
				Status:  v1.ErrorUpgradeRequired,
				Message: "Please, update the app",
			})
		}
	} else if appPlatform == "ios" {
		if appV.LessThan(iosMinVersion) {
			c.AbortWithStatusJSON(http.StatusUpgradeRequired, models.ServerError{
				Status:  v1.ErrorUpgradeRequired,
				Message: "Please, update the app",
			})
		}
	}
}
