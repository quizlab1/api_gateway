package models

//LoginModel ...
type LoginModel struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	FmcToken string `json:"fcm_token"`
}

//LoginResponseModel ...
type LoginResponseModel struct {
	ID           string `json:"id" example:"965b0929-82e1-4a53-ad0b-a16f50c99573"`
	Email        string `json:"email"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Username     string `json:"username"`
	ProfilePhoto string `json:"profile_photo"`
}

//GetNewTokensModel ...
type GetNewTokensModel struct {
	ID           string `json:"id"`
	RefreshToken string `json:"refresh_token"`
}

//GetNewTokensResponse ...
type GetNewTokensResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}
