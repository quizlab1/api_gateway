package models

import (
	"regexp"

	validation "github.com/go-ozzo/ozzo-validation/v3"
	"github.com/go-ozzo/ozzo-validation/v3/is"
)

// RegisterModel is a representation of a person
type RegisterModel struct {
	Email string `json:"email"`
	Password string `json:"password"`
}

// Validate Register Model
func (rm *RegisterModel) Validate() error {
	return validation.ValidateStruct(
		rm,
		validation.Field(&rm.Email, validation.Required, is.Email),
		validation.Field(&rm.Password, validation.Required, validation.Length(8, 30), validation.Match(regexp.MustCompile("[a-z]|[A-Z][0-9]"))),
	)
}

// UserDetail ...
type UserDetail struct {
	FirstName string `json:"first_name"`
	LastName string `json:"last_name"`
	UserName string `json:"username"`
	Email string `json:"email"`
	Password string `json:"password"`
}

// Validate Register Model
func (rm *UserDetail) Validate() error {
	return validation.ValidateStruct(
		rm,
		validation.Field(&rm.Email, validation.Required, is.Email),
		validation.Field(&rm.Password, validation.Required, validation.Length(8, 30), validation.Match(regexp.MustCompile("[a-z]|[A-Z][0-9]"))),
	)
}

//Email ...
type Email struct {
	Email string `json:"email"`
	Type  string `json:"type"`
}

//Validate Email Model
func (rm *Email) Validate() error {
	return validation.ValidateStruct(
		rm,
		validation.Field(&rm.Email, validation.Required, is.Email),
	)
}

//UpdateEmail ...
type UpdateEmail struct {
	Email string `json:"email"`
}

//Validate Email Model
func (rm *UpdateEmail) Validate() error {
	return validation.ValidateStruct(
		rm,
		validation.Field(&rm.Email, validation.Required, is.Email),
	)
}

//SetPassword ...
type SetPassword struct {
	ID          string `json:"id"`
	Code        string `json:"code"`
	Email       string `json:"email"`
	NewPassword string `json:"new_password"`
}

//Validate Set Password Model
func (sp *SetPassword) Validate() error {
	return validation.ValidateStruct(
		sp,
		validation.Field(&sp.NewPassword, validation.Required, validation.Length(8, 30), validation.Match(regexp.MustCompile("[a-z]|[A-Z][0-9]"))),
	)
}

//SetPasswordNew ...
type SetPasswordNew struct {
	NewPassword string `json:"new_password"`
}

//Validate Set Password Model
func (spn *SetPasswordNew) Validate() error {
	return validation.ValidateStruct(
		spn,
		validation.Field(&spn.NewPassword, validation.Required, validation.Length(8, 30), validation.Match(regexp.MustCompile("[a-z]|[A-Z][0-9]"))),
	)
}

//RegisterUserModel ...
type RegisterUserModel struct {
	ID           string `json:"id"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	UserName     string `json:"username"`
	Email        string `json:"email"`
	Bio          string `json:"bio"`
	ProfilePhoto string `json:"profile_photo"`
	FcmToken     string `json:"fcm_token"`
}

//Validate Register User Model
func (rum *RegisterUserModel) Validate() error {
	return validation.ValidateStruct(
		rum,
		validation.Field(&rum.FirstName, validation.Required, validation.Length(1, 30)),
		validation.Field(&rum.LastName, validation.Required, validation.Length(1, 30)),
		validation.Field(&rum.UserName, validation.Required, validation.Length(5, 30), validation.Match(regexp.MustCompile("^[0-9a-z_.]+$"))),
		validation.Field(&rum.Email, validation.Required, is.Email),
		validation.Field(&rum.Bio, validation.Length(0, 150)),
	)
}

//SocialRegisterModel ...
type SocialRegisterModel struct {
	ID           string `json:"id"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	UserName     string `json:"username"`
	Email        string `json:"email"`
	ProfilePhoto string `json:"profile_photo"`
	FcmToken     string `json:"fcm_token"`
}

//Validate Register User Model
func (rum *SocialRegisterModel) Validate() error {
	return validation.ValidateStruct(
		rum,
		validation.Field(&rum.FirstName, validation.Required, validation.Length(1, 30)),
		validation.Field(&rum.LastName, validation.Required, validation.Length(1, 30)),
		validation.Field(&rum.UserName, validation.Required, validation.Length(5, 30), validation.Match(regexp.MustCompile("^[0-9a-z_.]+$"))),
		validation.Field(&rum.Email, is.Email),
	)
}

//SendCodeResponse ...
type SendCodeResponse struct {
	ID string `json:"id"`
}

//RegisterResponseModel ...
type RegisterResponseModel struct {
	ID           string `json:"id" example:"965b0929-82e1-4a53-ad0b-a16f50c99573"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

//RegisterConfirmModel ...
type RegisterConfirmModel struct {
	Email string `json:"email"`
	Code  string `json:"code"`
}

//RegisterConfirmResponseModel ...
type RegisterConfirmResponseModel struct {
	Answer     string `json:"answer"`
	IsVerified bool   `json:"is_verified"`
}

//RegisterRespModel ...
type RegisterRespModel struct {
	Result string `json:"result"`
}

//DataUser ...
type DataUser struct {
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	UserName     string `json:"username"`
	Email        string `json:"email"`
	Bio          string `json:"bio"`
	ProfilePhoto string `json:"profile_photo"`
	FcmToken     string `json:"fcm_token"`
	Password     string `json:"password"`
	Code         string `json:"code"`
	Gender       string `json:"gender"`
	BirthDate    string `json:"birth_date"`
	PhoneNumber  string `json:"phone_number"`
}

//RegisterUserResponseModel ...
type RegisterUserResponseModel struct {
	ID           string `json:"id"`
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	UserName     string `json:"username"`
	Email        string `json:"email"`
	Bio          string `json:"bio"`
	ProfilePhoto string `json:"profilePhoto"`
	FcmToken     string `json:"fcmToken"`
}

//FacebookRegisterModel ...
type FacebookRegisterModel struct {
	ID           string `json:"id"`
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	UserName     string `json:"username"`
	Email        string `json:"email"`
	Bio          string `json:"bio"`
	Phone        string `json:"phone"`
	ProfilePhoto string `json:"profilePhoto"`
	FcmToken     string `json:"fcmToken"`
	BirthDate    string `json:"birth_date"`
}

//InstConfig ...
type InstConfig struct {
	RedirectURI string
	AppID       string
	AppSecret   string
}

//InstagramAccessTokenResponse ...
type InstagramAccessTokenResponse struct {
	UserID      uint64 `json:"user_id"`
	AccessToken string `json:"access_token"`
}

//FbLoginRequest ...
type FbLoginRequest struct {
	AccessToken string `json:"access_token"`
	FbID        string `json:"facebook_id"`
	FcmToken    string `json:"fcm_token"`
}

//GoogleLoginRequest ...
type GoogleLoginRequest struct {
	Email       string `json:"email"`
	AccessToken string `json:"access_token"`
	GoogleID    string `json:"google_id"`
	FcmToken    string `json:"fcm_token"`
}

//AppleLoginModel ...
type AppleLoginModel struct {
	Token string `json:"access_token"`
	SPA   bool   `json:"spa"`
}

// AppleTestLogin ...
type AppleTestLogin struct {
	TeamID      string `json:"team_id"`
	ClientID    string `json:"client_id"`
	RedirectURI string `json:"redirect_uri"`
	KeyID       string `json:"key_id"`
	Code        string `json:"code"`
}

//Data ...
type Data struct {
	Height       string `json:"height"`
	IsSilhouette bool   `json:"is_silhouette"`
	URL          string `json:"url"`
	Width        int    `json:"width"`
}

//Picture ...
type Picture struct {
	Data Data `json:"data"`
}

//FbLoginResponse ...
type FbLoginResponse struct {
	ID           string  `json:"id"`
	FirstName    string  `json:"first_name"`
	LastName     string  `json:"last_name"`
	Username     string  `json:"username"`
	Email        string  `json:"email"`
	AccessToken  string  `json:"access_token"`
	RefreshToken string  `json:"refresh_token"`
	Picture      Picture `json:"picture"`
	Registered   bool    `json:"registered"`
}

//GoogleLoginResponse ...
type GoogleLoginResponse struct {
	ID           string `json:"id"`
	GivenName    string `json:"given_name"`
	FamilyName   string `json:"family_name"`
	Username     string `json:"username"`
	Birthday     string `json:"birthday"`
	Website      string `json:"website"`
	Email        string `json:"email"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	Picture      string `json:"picture"`
	Registered   bool   `json:"registered"`
	BirthDate    string `json:"birth_date"`
}

//AppleUser ...
type AppleUser struct {
	ID            string `json:"sub,omitempty"`
	Email         string `json:"email,omitempty"`
	EmailVerified bool   `json:"email_verified,string,omitempty"`
}

//AppleLoginResponse ...
type AppleLoginResponse struct {
	ID           string `json:"id"`
	Email        string `json:"email"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	Registered   bool   `json:"registered"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Username     string `json:"username"`
}

// VerifyResModel ...
type VerifyResModel struct {
	ID           string `json:"id"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

// RegisterResModel ...
type RegisterResModel struct {
	Result string `json:"result"`
}
