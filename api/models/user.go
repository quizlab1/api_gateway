package models

type (
	//GetTotalScore ...
	GetTotalScore struct {
		TotalScore int64 `json:"total_score"`
	}

	//SetTotalScore ...
	SetTotalScore struct {
		TotalScore int64 `json:"total_score"`
	}

	//SuccessMessage ...
	SuccessMessage struct {
		Message string `json:"message"`
	}

	//GetProfileResponseModel ...
	GetProfileResponseModel struct {
		ID           string `json:"id"`
		FirstName    string `json:"first_name"`
		LastName     string `json:"last_name"`
		UserName     string `json:"username"`
		Rating       int64  `json:"rating"`
		TotalScore   int64	`json:"total_score"`
		ProfilePhoto string `json:"profile_photo"`
	}

	//Leader ...
	Leader struct {
		ID           string `json:"id" example:"965b0929-82e1-4a53-ad0b-a16f50c99573"`
		FirstName    string `json:"first_name"`
		LastName     string `json:"last_name"`
		Username     string `json:"username"`
		ProfilePhoto string `json:"profile_photo"`
		Rating       int64  `json:"rating"`
	}

	//GetLeadersListModel ...
	GetLeadersListModel struct {
		Leaders []Leader `json:"leaders"`
		Count   uint64   `json:"count"`
	}
)
