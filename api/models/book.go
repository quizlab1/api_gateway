package models

//Book ...
type Book struct {
	ID        string `json:"id" example:"965b0929-82e1-4a53-ad0b-a16f50c99573"`
	Title     string `json:"title"`
	GenreID   string `json:"genre_id"`
	BookImage string `json:"book_image"`
	Soon      string `json:"soon"`
	IsNew     bool   `json:"is_new"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

//Genre ...
type Genre struct {
	ID    int64  `json:"id"`
	Title string `json:"title"`
	Code  string `json:"code"`
}

//GetAllBooksModel ...
type GetAllBooksModel struct {
	Book  []Book `json:"books"`
	Count uint64 `json:"count"`
}

//GetGenresModel ...
type GetGenresModel struct {
	Genres []Genre `json:"genres"`
	Count int64   `json:"count"`
}

//Question ...
type Question struct {
	ID      int    `json:"id"`
	OptionA string `json:"option_a"`
	OptionB string `json:"option_b"`
	OptionC string `json:"option_c"`
	OptionD string `json:"option_d"`
	BookID  int    `json:"book_id"`
}

//ListQuestionsResp ...
type ListQuestionsResp struct {
	Count     int        `json:"count"`
	Questions []Question `json:"questions,omitempty"`
}
