package v1

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/gomodule/redigo/redis"
	"gitlab.com/quizlab/api_gateway/api/models"
	notifpb "gitlab.com/quizlab/api_gateway/genproto/notification_service"
	pb "gitlab.com/quizlab/api_gateway/genproto/user_service"
	"gitlab.com/quizlab/api_gateway/pkg/etc"
	"gitlab.com/quizlab/api_gateway/pkg/logger"
	"gitlab.com/quizlab/api_gateway/pkg/utils"
)

// SendVerification ...
// @Summary Send Verification
// @Description Send Verification link - API for sending Verification to check email while resetting password
// @Tags login
// @Accept  json
// @Produce  json
// @Param email body models.Email true "email"
// @Success 200 {object} models.SendCodeResponse
// @Failure 400 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/send-verification/ [post]
func (h *HandlerV1) SendVerification(c *gin.Context) {
	var (
		model  models.Email
		code   string
		result map[string]interface{}
	)

	err := c.ShouldBindJSON(&model)
	if handleBadRequestErrWithMessage(c, h.log, err, "Error binding json") {
		return
	}

	model.Email = strings.TrimSpace(model.Email)
	model.Email = strings.ToLower(model.Email)

	err = model.Validate()
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeInvalidJSON,
				Message: err.Error(),
			},
		})
		h.log.Error("Error while validating", logger.Error(err))

		return
	}

	checkEmail, err := h.grpcClient.UserService().CheckField(
		context.Background(), &pb.CheckFieldRequest{
			Field: "email",
			Value: model.Email,
		},
	)
	if handleGrpcErrWithMessage(c, h.log, err, fmt.Sprintf("Failed to check uniqueness of email: %v", model.Email)) {
		return
	}

	if !checkEmail.Exists {
		c.JSON(http.StatusConflict, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeAlreadyExists,
				Message: "User not found with this email",
			},
		})
		return
	}

	code = etc.GenerateCode(6)
	if handleBadRequestErrWithMessage(c, h.log, err, "Error while generating url link") {
		return
	}

	var linkForFirebase string
	if h.cfg.Environment == "develop" {
		linkForFirebase = "https://facetap.test.uzpos.uz/v1/verify/" + model.Email + "/" + code + "/" + model.Type + "/"
	} else if h.cfg.Environment == "staging" {
		linkForFirebase = "https://facetap.test.uzpos.uz/v1/verify/" + model.Email + "/" + code + "/" + model.Type + "/"
	} else {
		linkForFirebase = "https://facetap.test.uzpos.uz/v1/verify/" + model.Email + "/" + code + "/" + model.Type + "/"
	}
	url := "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=" + h.cfg.FirebaseWebKey
	var jsonStr = []byte(`{
		"dynamicLinkInfo": {
		  "domainUriPrefix": "`+ h.cfg.DomainURIPrefix + `",
		  "link": "` + linkForFirebase + `",
		  "androidInfo": {
			"androidPackageName": "`+ h.cfg.AndroidPackageName +`"
		  },
		  "iosInfo": {
			"iosBundleId": "`+ h.cfg.IosBundleID +`"
		  }
		}
	  }`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	json.NewDecoder(resp.Body).Decode(&result)
	link := fmt.Sprintf("%s", result["shortLink"])

	emailBody, err := utils.ParseTemplate("./html/resetpass.html", map[string]string{"Link": link})
	if handleGrpcErrWithMessage(c, h.log, err, "Error while parsing html template") {
		return
	}

	_, err = h.grpcClient.NotificationService().SendEmail(
		context.Background(), &notifpb.SendEmailRequest{
			Subject:    "Link for verification",
			Body:       emailBody,
			Recipients: []string{model.Email},
		},
	)
	if handleGrpcErrWithMessage(c, h.log, err, "Error while sending email") {
		return
	}

	data := models.DataUser{
		Code:  code,
		Email: model.Email,
	}

	bodyJSON, err := json.Marshal(data)
	if handleBadRequestErrWithMessage(c, h.log, err, "Error marshalling req") {
		return
	}

	err = h.inMemoryStorage.SetWithTTL(model.Email, string(bodyJSON), 86400)
	if handleInternalWithMessage(c, h.log, err, "Error while setting user data") {
		return
	}

	user := models.UserDetail{}

	userJSON, err := redis.String(h.inMemoryStorage.Get(model.Email))
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal([]byte(userJSON), &user)
	if err != nil {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	c.JSON(http.StatusOK, "Verification link has been sent to your email, please check and verify")
}

// SetPassword ...
// @Security ApiKeyAuth
// @Summary SetPassword
// @Description Set Password - API for setting password
// @Tags login
// @Accept  json
// @Produce  json
// @Param set_password body models.SetPasswordNew true "set password (new)"
// @Success 200
// @Failure 400 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/set-password/ [post]
func (h *HandlerV1) SetPassword(c *gin.Context) {

	var sp models.SetPassword

	err := c.ShouldBindJSON(&sp)
	if handleBadRequestErrWithMessage(c, h.log, err, "Error binding json") {
		return
	}

	sp.NewPassword = strings.TrimSpace(sp.NewPassword)

	err = sp.Validate()
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeInvalidJSON,
				Message: "Password must be between 8 and 30",
			},
		})
		h.log.Error("Error while validating", logger.Error(err))

		return
	}

	hashedPwd, err := etc.GeneratePasswordHash(sp.NewPassword)
	if handleInternalWithMessage(c, h.log, err, "Error generating hash for password") {
		return
	}

	claims := GetClaims(h, c)

	_, err = h.grpcClient.UserService().SetPassword(
		context.Background(), &pb.SetPasswordReq{
			Id:       claims["sub"].(string),
			Password: string(hashedPwd),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeInvalidJSON,
				Message: err.Error(),
			},
		})
		h.log.Error("Error while validating", logger.Error(err))

		return
	}

	c.Status(http.StatusOK)
}
