package v1

import (
	// "fmt"
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq" //for postgres drivers
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/quizlab/api_gateway/api/models"
	"gitlab.com/quizlab/api_gateway/api/token"
	pb "gitlab.com/quizlab/api_gateway/genproto/user_service"
	"gitlab.com/quizlab/api_gateway/pkg/logger"
)

// Login ...
// @Summary Login
// @Description Login API is for signing in to app
// @Description It also refreshes both tokens
// @Tags login
// @Accept  json
// @Produce  json
// @Param login body models.LoginModel true "login"
// @Success 200 {object} models.LoginResponseModel true "login response"
// @Failure 400 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/login/ [post]
func (h *HandlerV1) Login(c *gin.Context) {
	var (
		login                     models.LoginModel
		loginResponse             models.LoginResponseModel
		accessToken, refreshToken string
	)

	err := c.ShouldBindJSON(&login)
	if handleBadRequestErrWithMessage(c, h.log, err, "Error binding json") {
		return
	}

	res, err := h.grpcClient.UserService().Login(
		context.Background(), &pb.LoginReq{
			Email:    login.Email,
			Password: login.Password,
		},
	)

	message := "Error while getting client by email"

	st, ok := status.FromError(err)
	if !ok || st.Code() == codes.Internal {

		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeInternal,
				Message: st.Message(),
			},
		})
		h.log.Error(message, logger.Error(err))
		return
	} else if st.Code() == codes.NotFound {

		c.JSON(http.StatusNotFound, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeNotFound,
				Message: st.Message(),
			},
		})
		h.log.Error(message+", not found", logger.Error(err))
		return
	} else if st.Code() == codes.Unavailable {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeInternal,
				Message: "Internal Server Error",
			},
		})
		h.log.Error(message+", service unavailable", logger.Error(err))
		return
	} else if st.Code() == codes.InvalidArgument {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorInvalidCredentials,
				Message: st.Message(),
			},
		})
		h.log.Error(message+", service unavailable", logger.Error(err))
		return
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorBadRequest,
				Message: err.Error(),
			},
		})
		return
	}

	h.jwtHandler = token.JWTHandler{
		SigninKey: h.cfg.SigninKey,
		Sub:       res.User.Id,
		Iss:       "user",
		Role:      "authorized",
		Aud: []string{
			"quizlab-frontend",
		},
		Log: h.log,
	}

	accessToken, refreshToken, err = h.jwtHandler.GenerateAuthJWT()
	if handleInternalWithMessage(c, h.log, err, "Error while generating tokens") {
		return
	}

	ucReq := &pb.UpdateUserTokensReq{
		Id:           res.User.Id,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}

	_, err = h.grpcClient.UserService().UpdateUserTokens(
		context.Background(), ucReq,
	)
	if handleGrpcErrWithMessage(c, h.log, err, "Error while updating client token", ucReq) {
		return
	}

	if login.FmcToken != "" {
		h.grpcClient.UserService().SetFcmToken(
			context.Background(), &pb.SetFcmTokenRequest{
				FcmToken: login.FmcToken,
				Id:       res.User.Id,
			},
		)
	}

	loginResponse.ID = res.User.Id
	loginResponse.Email = login.Email
	loginResponse.AccessToken = accessToken
	loginResponse.RefreshToken = refreshToken
	loginResponse.FirstName = res.User.FirstName
	loginResponse.LastName = res.User.LastName
	loginResponse.Username = res.User.Username
	loginResponse.ProfilePhoto = res.User.ProfilePhoto

	c.JSON(http.StatusOK, loginResponse)
	return
}

// GetNewTokens ...
// @Summary Get Tokens
// @Description Get Tokens API is for getting new tokens
// @Description if old one is expired
// @Tags login
// @Accept  json
// @Produce  json
// @Param get_tokens body models.GetNewTokensModel true "get tokens"
// @Success 200 {array} models.GetNewTokensResponse
// @Failure 404 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/get_tokens/ [post]
func (h *HandlerV1) GetNewTokens(c *gin.Context) {
	var getTokens models.GetNewTokensModel

	err := c.ShouldBindJSON(&getTokens)
	if handleBadRequestErrWithMessage(c, h.log, err, "Error binding json") {
		return
	}
	claims, err := token.ExtractClaim(getTokens.RefreshToken, []byte(h.cfg.SigninKey))
	if handleGrpcErrWithMessage(c, h.log, err, "Unauthorized request", claims) {
		return
	}

	client, err := h.grpcClient.UserService().GetAuthUser(
		context.Background(), &pb.GetAuthUserReq{
			Id: getTokens.ID,
		},
	)
	if handleGrpcErrWithMessage(c, h.log, err, "Error while getting user", getTokens.ID) {
		return
	}

	h.jwtHandler = token.JWTHandler{
		SigninKey: h.cfg.SigninKey,
		Sub:       client.Id,
		Iss:       "user",
		Role:      "authorized",
		Aud: []string{
			"quizlab-frontend",
		},
		Log: h.log,
	}

	accessToken, refreshToken, err := h.jwtHandler.GenerateAuthJWT()
	if handleInternalWithMessage(c, h.log, err, "Error while generating tokens") {
		return
	}

	req := &pb.UpdateUserTokensReq{
		Id:           getTokens.ID,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
	_, err = h.grpcClient.UserService().UpdateUserTokens(
		context.Background(), req,
	)
	if handleGrpcErrWithMessage(c, h.log, err, "Error while updating user tokens", req) {
		return
	}
	c.JSON(http.StatusOK, models.GetNewTokensResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	})
}

// Logout ...
// @Security ApiKeyAuth
// @Summary Logout
// @Description Logout API is for deleting fcm token from db
// @Tags login
// @Produce  json
// @Success 200
// @Failure 400 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/logout/ [put]
func (h *HandlerV1) Logout(c *gin.Context) {
	claims := GetClaims(h, c)
	_, err := h.grpcClient.UserService().Logout(
		context.Background(), &pb.LogoutRequest{
			Id: claims["sub"].(string),
		},
	)
	if handleGrpcErrWithMessage(c, h.log, err, "Error while loging out client", claims["sub"].(string)) {
		return
	}

	c.Status(http.StatusOK)
}
