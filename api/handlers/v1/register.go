package v1

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/google/uuid"
	"github.com/gin-gonic/gin"
	"github.com/gomodule/redigo/redis"
	"gitlab.com/quizlab/api_gateway/api/helpers"
	"gitlab.com/quizlab/api_gateway/api/models"
	"gitlab.com/quizlab/api_gateway/api/token"
	notifpb "gitlab.com/quizlab/api_gateway/genproto/notification_service"
	pb "gitlab.com/quizlab/api_gateway/genproto/user_service"
	"gitlab.com/quizlab/api_gateway/pkg/etc"
	"gitlab.com/quizlab/api_gateway/pkg/logger"
	"gitlab.com/quizlab/api_gateway/pkg/utils"
)

// Register ...
// @Summary Register
// @Description Register - API for registering users
// @Tags register
// @Accept  json
// @Produce  json
// @Param register body models.UserDetail true "register"
// @Success 200 {object} models.RegisterResModel
// @Failure 400 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/register/ [post]
func (h *HandlerV1) Register(c *gin.Context) {
	var (
		body   models.UserDetail
		code   string
		result map[string]interface{}
	)

	err := c.ShouldBindJSON(&body)
	if handleBadRequestErrWithMessage(c, h.log, err, "Error binding json") {
		return
	}

	body.Email = strings.TrimSpace(body.Email)
	body.Email = strings.ToLower(body.Email)

	err = body.Validate()
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeInvalidJSON,
				Message: err.Error(),
			},
		})
		h.log.Error("Error while validating", logger.Error(err))

		return
	}

	//Checking uniqueness of username
	checkUsername, err := h.grpcClient.UserService().CheckField(
		context.Background(), &pb.CheckFieldRequest{
			Field: "username",
			Value: body.UserName,
		},
	)
	if handleGrpcErrWithMessage(c, h.log, err, fmt.Sprintf("Failed to check uniqueness of username: %v", body.UserName)) {
		return
	}

	if checkUsername.Exists {
		c.JSON(http.StatusConflict, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeAlreadyExists,
				Message: "Username already exists",
			},
		})
		return
	}

	checkEmail, err := h.grpcClient.UserService().CheckField(
		context.Background(), &pb.CheckFieldRequest{
			Field: "email",
			Value: body.Email,
		},
	)
	if handleGrpcErrWithMessage(c, h.log, err, fmt.Sprintf("Failed to check uniqueness of email: %v", body.Email)) {
		return
	}

	if checkEmail.Exists {
		c.JSON(http.StatusConflict, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeAlreadyExists,
				Message: "Mail already exists",
			},
		})
		return
	}

	code = etc.GenerateCode(6)
	fmt.Println(code)
	if handleBadRequestErrWithMessage(c, h.log, err, "Error while generating url link") {
		return
	}

	var linkForFirebase string
	if h.cfg.Environment == "develop" {
		linkForFirebase = "https://quizlab.uzdevs.com/v1/verify/" + body.Email + "/" + code + "/signup/"
	} else if h.cfg.Environment == "staging" {
		linkForFirebase = "https://quizlab.uzdevs.com/v1/verify/" + body.Email + "/" + code + "/signup/"
	} else {
		linkForFirebase = "https://quizlab.uzdevs.com/v1/verify/" + body.Email + "/" + code + "/signup/"
	}
	url := "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=" + h.cfg.FirebaseWebKey
	var jsonStr = []byte(`{
		"dynamicLinkInfo": {
		  "domainUriPrefix": "` + h.cfg.DomainURIPrefix + `",
		  "link": "` + linkForFirebase + `",
		  "androidInfo": {
			"androidPackageName": "` + h.cfg.AndroidPackageName + `"
		  },
		  "iosInfo": {
			"iosBundleId": "` + h.cfg.IosBundleID + `"
		  }
		}
	  }`)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	json.NewDecoder(resp.Body).Decode(&result)
	link := fmt.Sprintf("%s", result["shortLink"])
	fmt.Println(url)
	fmt.Println(result["shortLink"])
	fmt.Println(link)

	emailBody, err := utils.ParseTemplate("./html/email.html", map[string]string{"Link": link, "first_name": body.FirstName, "last_name": body.LastName})
	if handleGrpcErrWithMessage(c, h.log, err, "Error while parsing html template") {
		return
	}

	_, err = h.grpcClient.NotificationService().SendEmail(
		context.Background(), &notifpb.SendEmailRequest{
			Subject:    "Link for verification",
			Body:       emailBody,
			Recipients: []string{body.Email},
		},
	)
	if handleGrpcErrWithMessage(c, h.log, err, "Error while sending email") {
		return
	}

	fmt.Println(code)
	data := models.DataUser{
		FirstName: body.FirstName,
		LastName:  body.LastName,
		Code:      code,
		UserName:  body.UserName,
		Password:  body.Password,
		Email:     body.Email,
	}

	bodyJSON, err := json.Marshal(data)
	if handleBadRequestErrWithMessage(c, h.log, err, "Error marshalling req") {
		return
	}

	err = h.inMemoryStorage.SetWithTTL(body.Email, string(bodyJSON), 86400)
	if handleInternalWithMessage(c, h.log, err, "Error while setting user data") {
		return
	}

	user := models.UserDetail{}

	userJSON, err := redis.String(h.inMemoryStorage.Get(body.Email))
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal([]byte(userJSON), &user)
	if err != nil {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	c.JSON(http.StatusOK, models.RegisterRespModel{
		Result: "Verification link has been sent to your email, please check and verify",
	})
}

// Verify ...
// @Summary Verify
// @Description returns access token. type=signup/forgot/email_update
// @Tags register
// @Accept  json
// @Produce  json
// @Param email path string true "email"
// @Param code path string true "code"
// @Param type path string true "signup/forgot/email_update"
// @Success 200 {object} models.VerifyResModel
// @Failure 404 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/verify/{email}/{code}/{type}/ [get]
func (h *HandlerV1) Verify(c *gin.Context) {

	email := c.Param("email")
	code := c.Param("code")
	vType := c.Param("type")

	if !helpers.InEnums(vType, []string{"signup", "forgot", "email_update"}) {
		c.JSON(http.StatusInternalServerError, models.StandardErrorModel{
			Error: models.Error{
				Status:  ErrorBadRequest,
				Message: "Invalid value 'object_type'",
			},
		})
	}

	//Getting code from redis
	user := models.DataUser{}

	userJSON, err := redis.String(h.inMemoryStorage.Get(email))
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal([]byte(userJSON), &user)
	if err != nil {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	//Checking whether received code is valid
	fmt.Println(code)
	fmt.Print(user.Code)
	if code != user.Code {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.InternalServerError{
				Status: ErrorCodeInvalidCode,
			},
		})
		h.log.Error("verification failed", logger.Error(err))
		return
	}

	id, err := uuid.NewRandom()
	if handleInternalWithMessage(c, h.log, err, "Error while generating UUID") {
		return
	}

	if vType == "signup" {
		h.jwtHandler = token.JWTHandler{
			SigninKey: h.cfg.SigninKey,
			Sub:       id.String(),
			Iss:       "user",
			Role:      "authorized",
			Aud: []string{
				"quizlab-frontend",
			},
			Log: h.log,
		}

		//Creating access and refresh tokens
		accessTokenString, refreshTokenString, err := h.jwtHandler.GenerateAuthJWT()
		if handleInternalWithMessage(c, h.log, err, "Error while generating tokens") {
			return
		}

		//Creating hash of a password
		hashedPassword, err := etc.GeneratePasswordHash(user.Password)
		if handleInternalWithMessage(c, h.log, err, "Error generating hash for password") {
			return
		}

		checkEmail, err := h.grpcClient.UserService().CheckField(
			context.Background(), &pb.CheckFieldRequest{
				Field: "email",
				Value: user.Email,
			},
		)
		if handleGrpcErrWithMessage(c, h.log, err, fmt.Sprintf("Failed to check uniqueness of email: %v", user.Email)) {
			return
		}

		if checkEmail.Exists {
			c.JSON(http.StatusConflict, models.ResponseError{
				Error: models.InternalServerError{
					Status:  ErrorCodeAlreadyExists,
					Message: "Mail already exists",
				},
			})
			return
		}

		// 	//Creating new user
		resUser, err := h.grpcClient.UserService().CreateAuthUser(context.Background(), &pb.User{
			Id:           id.String(),
			Email:        user.Email,
			Password:     string(hashedPassword),
			AccessToken:  accessTokenString,
			RefreshToken: refreshTokenString,
			FirstName:    user.FirstName,
			LastName:     user.LastName,
			Username:     user.UserName,
		})
		if handleGrpcErrWithMessage(c, h.log, err, "Error while creating a client", user) {
			return
		}

		c.JSON(http.StatusOK, &models.RegisterResponseModel{
			ID:           resUser.Id,
			AccessToken:  resUser.AccessToken,
			RefreshToken: resUser.RefreshToken,
		})
	} else if vType == "forgot" || vType == "email_update" {
		res, err := h.grpcClient.UserService().GetByEmail(
			context.Background(), &pb.GetByEmailReq{
				Email: email,
			},
		)
		if handleGrpcErrWithMessage(c, h.log, err, "Error while getting a client by email", user) {
			return
		}

		h.jwtHandler = token.JWTHandler{
			SigninKey: h.cfg.SigninKey,
			Sub:       res.User.Id,
			Iss:       "user",
			Role:      "authorized",
			Aud: []string{
				"quizlab-frontend",
			},
			Log: h.log,
		}

		accessToken, refreshToken, err := h.jwtHandler.GenerateAuthJWT()
		if handleInternalWithMessage(c, h.log, err, "Error while generating tokens") {
			return
		}

		ucReq := &pb.UpdateUserTokensReq{
			Id:           res.User.Id,
			AccessToken:  accessToken,
			RefreshToken: refreshToken,
		}

		_, err = h.grpcClient.UserService().UpdateUserTokens(
			context.Background(), ucReq,
		)
		if handleGrpcErrWithMessage(c, h.log, err, "Error while updating client token", ucReq) {
			return
		}

		c.JSON(http.StatusOK, &models.RegisterResponseModel{
			ID:           res.User.Id,
			AccessToken:  accessToken,
			RefreshToken: refreshToken,
		})
	}

}
