package v1

import (
	"context"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/jsonpb"
	"gitlab.com/quizlab/api_gateway/api/models"
	pb "gitlab.com/quizlab/api_gateway/genproto/book_service"
	"gitlab.com/quizlab/api_gateway/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetAllBooks ...
// @Security ApiKeyAuth
// @Summary Get all books
// @Description Get all books
// @Tags book
// @Accept  json
// @Produce  json
// @Param page query string false "Page"
// @Param limit query string false "Limit"
// @Success 200 {object} models.GetAllBooksModel
// @Failure 404 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/book/ [get]
func (h *HandlerV1) GetAllBooks(c *gin.Context) {
	var (
		jspbMarshal jsonpb.Marshaler
		// pageValue, limitValue string
		page, limit int
		// hasError              bool
		err error
		// users                 *pb.GetAllAuthUsersResp
	)

	jspbMarshal.EmitDefaults = true

	page, err = ParsePageQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: ErrorBadRequest,
		})
		h.log.Error("Error while parsing page", logger.Error(err))
		return
	}

	limit, err = ParseLimitQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: ErrorBadRequest,
		})
		h.log.Error("Error while parsing limit", logger.Error(err))
		return
	}

	claims := GetClaims(h, c)

	requests, err := h.grpcClient.BookService().GetAllBooks(
		context.Background(),
		&pb.GetAllBooksRequest{
			UserId: claims["sub"].(string),
			Limit:  uint64(limit),
			Page:   uint64(page),
		},
	)
	if err != nil {
		h.log.Error("Error while getting books with details", logger.Error(err))
	}

	st, ok := status.FromError(err)
	if !ok || st.Code() == codes.Internal {
		c.JSON(http.StatusInternalServerError, models.StandardErrorModel{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while getting all requests", logger.Error(err))
		return
	}
	if st.Code() == codes.Unavailable {
		c.JSON(http.StatusInternalServerError, models.StandardErrorModel{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while getting all books, service unavailable", logger.Error(err))
		return
	}

	jspbMarshal.OrigName = true

	js, err := jspbMarshal.MarshalToString(requests)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while marshalling", logger.Error(err))
		return
	}

	c.Header("Content-Type", "application/json")
	c.String(http.StatusOK, js)
}

// GetAllSoonBooks ...
// @Security ApiKeyAuth
// @Summary Get all soon books
// @Description Get all soon books
// @Tags book
// @Accept  json
// @Produce  json
// @Param page query string false "Page"
// @Param limit query string false "Limit"
// @Success 200 {object} models.GetAllBooksModel
// @Failure 404 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/book-soon/ [get]
func (h *HandlerV1) GetAllSoonBooks(c *gin.Context) {
	var (
		jspbMarshal jsonpb.Marshaler
		// pageValue, limitValue string
		page, limit int
		// hasError              bool
		err error
		// users                 *pb.GetAllAuthUsersResp
	)

	jspbMarshal.EmitDefaults = true

	page, err = ParsePageQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: ErrorBadRequest,
		})
		h.log.Error("Error while parsing page", logger.Error(err))
		return
	}

	limit, err = ParseLimitQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: ErrorBadRequest,
		})
		h.log.Error("Error while parsing limit", logger.Error(err))
		return
	}

	claims := GetClaims(h, c)

	requests, err := h.grpcClient.BookService().GetAllSoonBooks(
		context.Background(),
		&pb.GetAllBooksRequest{
			UserId: claims["sub"].(string),
			Limit:  uint64(limit),
			Page:   uint64(page),
		},
	)
	if err != nil {
		h.log.Error("Error while getting books with details", logger.Error(err))
	}

	st, ok := status.FromError(err)
	if !ok || st.Code() == codes.Internal {
		c.JSON(http.StatusInternalServerError, models.StandardErrorModel{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while getting all requests", logger.Error(err))
		return
	}
	if st.Code() == codes.Unavailable {
		c.JSON(http.StatusInternalServerError, models.StandardErrorModel{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while getting all books, service unavailable", logger.Error(err))
		return
	}

	jspbMarshal.OrigName = true

	js, err := jspbMarshal.MarshalToString(requests)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while marshalling", logger.Error(err))
		return
	}

	c.Header("Content-Type", "application/json")
	c.String(http.StatusOK, js)
}

// GetGenres ...
// @Security ApiKeyAuth
// @Summary Get all genres
// @Description Get all genres
// @Tags book
// @Accept  json
// @Produce  json
// @Param page query string false "Page"
// @Param limit query string false "Limit"
// @Success 200 {object} models.GetGenresModel
// @Failure 404 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/genres/ [get]
func (h *HandlerV1) GetGenres(c *gin.Context) {
	var (
		jspbMarshal jsonpb.Marshaler
		// pageValue, limitValue string
		page, limit int
		// hasError              bool
		err error
		// users                 *pb.GetAllAuthUsersResp
	)

	jspbMarshal.EmitDefaults = true

	page, err = ParsePageQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: ErrorBadRequest,
		})
		h.log.Error("Error while parsing page", logger.Error(err))
		return
	}

	limit, err = ParseLimitQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: ErrorBadRequest,
		})
		h.log.Error("Error while parsing limit", logger.Error(err))
		return
	}

	requests, err := h.grpcClient.BookService().GetGenres(
		context.Background(),
		&pb.GetGenresRequest{
			Limit: int64(limit),
			Page:  int64(page),
		},
	)
	if err != nil {
		h.log.Error("Error while getting genres with details", logger.Error(err))
	}

	st, ok := status.FromError(err)
	if !ok || st.Code() == codes.Internal {
		c.JSON(http.StatusInternalServerError, models.StandardErrorModel{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while getting all requests", logger.Error(err))
		return
	}
	if st.Code() == codes.Unavailable {
		c.JSON(http.StatusInternalServerError, models.StandardErrorModel{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while getting all genres, service unavailable", logger.Error(err))
		return
	}

	jspbMarshal.OrigName = true

	js, err := jspbMarshal.MarshalToString(requests)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while marshalling", logger.Error(err))
		return
	}

	c.Header("Content-Type", "application/json")
	c.String(http.StatusOK, js)
}

// ListQuestionsByBook ...
// @Router /v1/questions/{book_id}/ [get]
// @Security ApiKeyAuth
// @Tags book
// @Accept json
// @Produce json
// @Param book_id path string true "Book ID"
// @Success 200 {object} models.ListQuestionsResp
// @Failure 400 {object} models.StandardErrorModel
// @Failure 403
// @Failure 500 {object} models.StandardErrorModel
func (h *HandlerV1) ListQuestionsByBook(c *gin.Context) {

	response, err := h.grpcClient.BookService().GetAllQuestionByBook(
		context.Background(), &pb.GetAllQuestionByBookRequest{
			BookId: c.Param("book_id"),
		},
	)
	if handleGrpcErrWithMessage(c, h.log, err, "failed to list questions", c.Param("book_id")) {
		return
	}

	c.JSON(http.StatusOK, response)
}

// GetBookByGenre ...
// @Security ApiKeyAuth
// @Summary Get all books by genre
// @Description Get all books by genre
// @Tags book
// @Accept  json
// @Produce  json
// @Param page query string false "Page"
// @Param limit query string false "Limit"
// @Param genre_id path int true "Genre ID"
// @Success 200 {object} models.GetAllBooksModel
// @Failure 404 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/book/{genre_id}/ [get]
func (h *HandlerV1) GetBookByGenre(c *gin.Context) {
	var (
		jspbMarshal jsonpb.Marshaler
		// pageValue, limitValue string
		page, limit int
		// hasError              bool
		err error
		// users                 *pb.GetAllAuthUsersResp
	)

	jspbMarshal.EmitDefaults = true

	page, err = ParsePageQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: ErrorBadRequest,
		})
		h.log.Error("Error while parsing page", logger.Error(err))
		return
	}

	limit, err = ParseLimitQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: ErrorBadRequest,
		})
		h.log.Error("Error while parsing limit", logger.Error(err))
		return
	}

	genreID, err := strconv.ParseInt(c.Param("genre_id"), 10, 64)
	if err != nil {
		h.log.Error("Error while getting books with details", logger.Error(err))
	}

	requests, err := h.grpcClient.BookService().GetBooksByGenre(
		context.Background(),
		&pb.GetBooksByGenreRequest{
			GenreID: genreID,
			Limit:   int64(limit),
			Page:    int64(page),
		},
	)
	if err != nil {
		h.log.Error("Error while getting books with details", logger.Error(err))
	}

	st, ok := status.FromError(err)
	if !ok || st.Code() == codes.Internal {
		c.JSON(http.StatusInternalServerError, models.StandardErrorModel{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while getting all requests", logger.Error(err))
		return
	}
	if st.Code() == codes.Unavailable {
		c.JSON(http.StatusInternalServerError, models.StandardErrorModel{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while getting all books, service unavailable", logger.Error(err))
		return
	}

	jspbMarshal.OrigName = true

	js, err := jspbMarshal.MarshalToString(requests)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while marshalling", logger.Error(err))
		return
	}

	c.Header("Content-Type", "application/json")
	c.String(http.StatusOK, js)
}
