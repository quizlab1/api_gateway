package v1

import (
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/jsonpb"
	"gitlab.com/quizlab/api_gateway/api/models"
	pb "gitlab.com/quizlab/api_gateway/genproto/user_service"
	"gitlab.com/quizlab/api_gateway/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetTotalScore ...
// @Router /v1/total-score/ [get]
// @Security ApiKeyAuth
// @Tags user
// @Accept json
// @Produce json
// @Success 200 {object} models.GetTotalScore
// @Failure 500 {object} models.StandardErrorModel
func (h *HandlerV1) GetTotalScore(c *gin.Context) {
	claims := GetClaims(h, c)

	request := &pb.GetTotalScoreRequest{UserId: claims["sub"].(string)}

	resp, err := h.grpcClient.UserService().GetTotalScore(context.Background(), request)
	if !handleGRPCErr(c, h.log, err) {
		c.JSON(http.StatusOK, models.GetTotalScore{
			TotalScore: resp.GetTotalScore(),
		})
	}
}

// SetTotalScore ...
// @Router /v1/set-total-score/ [put]
// @Security ApiKeyAuth
// @Summary Update total score
// @Description this API updates total score of the user
// @Tags user
// @Accept json
// @Produce json
// @Param delete body models.SetTotalScore true "total score"
// @Success 200 {object} models.SuccessMessage
// @Failure 404 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
func (h *HandlerV1) SetTotalScore(c *gin.Context) {
	var (
		body        models.SetTotalScore
		jspbMarshal jsonpb.Marshaler
	)
	jspbMarshal.OrigName = true

	err := c.ShouldBindJSON(&body)
	if handleBadRequestErrWithMessage(c, h.log, err, "Error binding json") {
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	claims := GetClaims(h, c)

	_, err = h.grpcClient.UserService().SetTotalScore(
		ctx, &pb.SetTotalScoreRequest{
			TotalScore: body.TotalScore,
			UserId:     claims["sub"].(string),
		},
	)
	if handleGrpcErrWithMessage(c, h.log, err, "failed to update total score") {
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Edited successfully"})
}

// GetProfile ...
// @Security ApiKeyAuth
// @Summary Get Profile
// @Description Get Profile API returns a profile of a user
// @Tags user
// @Accept  json
// @Produce  json
// @Param user_id path string true "User ID"
// @Success 200 {object} models.GetProfileResponseModel
// @Failure 404 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/users/{user_id}/profile/ [get]
func (h *HandlerV1) GetProfile(c *gin.Context) {
	var (
		id  string
		ID  string
		err error
	)

	ID = c.Param("user_id")

	user, err := h.grpcClient.UserService().GetAuthUser(
		context.Background(), &pb.GetAuthUserReq{
			Id: ID,
		},
	)
	if handleGrpcErrWithMessage(c, h.log, err, "Error while getting user", id) {
		return
	}

	rating, err := h.grpcClient.UserService().GetUserRating(
		context.Background(), &pb.GetUserRatingRequest{
			UserId: ID,
		},
	)
	if handleGrpcErrWithMessage(c, h.log, err, "Error while getting users rating", id) {
		return
	}

	c.JSON(http.StatusOK, models.GetProfileResponseModel{
		ID:           user.Id,
		FirstName:    user.FirstName,
		LastName:     user.LastName,
		UserName:     user.Username,
		Rating:       int64(rating.Rating),
		ProfilePhoto: user.ProfilePhoto,
	})
}

// GetLeadersList ...
// @Security ApiKeyAuth
// @Summary Get all leaders
// @Description Get all leaders
// @Tags user
// @Accept  json
// @Produce  json
// @Param page query string false "Page"
// @Param limit query string false "Limit"
// @Success 200 {object} models.GetLeadersListModel
// @Failure 404 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /v1/leaders/ [get]
func (h *HandlerV1) GetLeadersList(c *gin.Context) {
	var (
		jspbMarshal jsonpb.Marshaler
		// pageValue, limitValue string
		page, limit int
		// hasError              bool
		err error
	)

	jspbMarshal.EmitDefaults = true

	page, err = ParsePageQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: ErrorBadRequest,
		})
		h.log.Error("Error while parsing page", logger.Error(err))
		return
	}

	limit, err = ParseLimitQueryParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: ErrorBadRequest,
		})
		h.log.Error("Error while parsing limit", logger.Error(err))
		return
	}

	claims := GetClaims(h, c)

	requests, err := h.grpcClient.UserService().GetLeadersList(
		context.Background(),
		&pb.GetLeadersListRequest{
			UserId: claims["sub"].(string),
			Limit:  uint64(limit),
			Page:   uint64(page),
		},
	)
	if err != nil {
		h.log.Error("Error while getting leaders with details", logger.Error(err))
	}

	st, ok := status.FromError(err)
	if !ok || st.Code() == codes.Internal {
		c.JSON(http.StatusInternalServerError, models.StandardErrorModel{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while getting all requests", logger.Error(err))
		return
	}
	if st.Code() == codes.Unavailable {
		c.JSON(http.StatusInternalServerError, models.StandardErrorModel{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while getting all leaders, service unavailable", logger.Error(err))
		return
	}

	jspbMarshal.OrigName = true

	js, err := jspbMarshal.MarshalToString(requests)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.Error{
				Status:  ErrorCodeInternal,
				Message: "Internal Server error",
			},
		})
		h.log.Error("Error while marshalling", logger.Error(err))
		return
	}

	c.Header("Content-Type", "application/json")
	c.String(http.StatusOK, js)
}
