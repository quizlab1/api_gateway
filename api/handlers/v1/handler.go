package v1

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	jwtg "github.com/dgrijalva/jwt-go"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	_ "gitlab.com/quizlab/api_gateway/api/docs" // for swagger
	"gitlab.com/quizlab/api_gateway/api/models"
	"gitlab.com/quizlab/api_gateway/api/token"
	"gitlab.com/quizlab/api_gateway/config"
	grpcclient "gitlab.com/quizlab/api_gateway/grpc_client"
	"gitlab.com/quizlab/api_gateway/pkg/logger"
	"gitlab.com/quizlab/api_gateway/storage/repo"
)

var (
	// MySigningKey ...
	MySigningKey = []byte("secretphrase")
	//NewSigningKey ...
	NewSigningKey = []byte("@!EqrttOLiwa(H8)7Ddo")
)

// HandlerV1 ..																																																																																																																																																																																										.
type HandlerV1 struct {
	inMemoryStorage repo.InMemoryStorageI
	log             logger.Logger
	grpcClient      grpcclient.I
	cfg             config.Config
	jwtHandler      token.JWTHandler
}

// New ...
func New(i repo.InMemoryStorageI, log logger.Logger, grpcClient grpcclient.I, cfg config.Config, jwtHandler token.JWTHandler) *HandlerV1 {
	return &HandlerV1{
		inMemoryStorage: i,
		cfg:             cfg,
		grpcClient:      grpcClient,
		log:             log,
		jwtHandler:      jwtHandler,
	}
}

const (
	//ErrorCodeInvalidURL ...
	ErrorCodeInvalidURL = "INVALID_URL"
	//ErrorCodeInvalidJSON ...
	ErrorCodeInvalidJSON = "INVALID_JSON"
	//ErrorCodeInvalidArgument ...
	ErrorCodeInvalidArgument = "INVALID_ARGUMENT"
	//ErrorCodeInvalidParams ...
	ErrorCodeInvalidParams = "INVALID_PARAMS"
	//ErrorCodeInternal ...
	ErrorCodeInternal = "INTERNAL"
	//ErrorCodeUnauthorized ...
	ErrorCodeUnauthorized = "UNAUTHORIZED"
	//ErrorCodeAlreadyExists ...
	ErrorCodeAlreadyExists = "ALREADY_EXISTS"
	//ErrorCodeNotFound ...
	ErrorCodeNotFound = "NOT_FOUND"
	//ErrorCodeInvalidCode ...
	ErrorCodeInvalidCode = "INVALID_CODE"
	//ErrorBadRequest ...
	ErrorBadRequest = "BAD_REQUEST"
	//ErrorCodeForbidden ...
	ErrorCodeForbidden = "FORBIDDEN"
	//ErrorCodeNotApproved ...
	ErrorCodeNotApproved = "NOT_APPROVED"
	// ErrorUpgradeRequired ...
	ErrorUpgradeRequired = "UPGRADE_REQUIRED"
	// ErrorUserBlocked ...
	ErrorUserBlocked = "USER_IS_BLOCKED_BY_ADMIN"
	// ErrorInvalidCredentials ...
	ErrorInvalidCredentials = "INVALID_CREDENTIALS"
)

// ParsePageQueryParam ...
func ParsePageQueryParam(c *gin.Context) (int, error) {
	page, err := strconv.Atoi(c.DefaultQuery("page", "1"))
	if err != nil {
		return 0, err
	}
	if page < 0 {
		return 0, errors.New("page must be an positive integer")
	}
	if page == 0 {
		return 1, nil
	}
	return page, nil
}

//ParseLimitQueryParam ...
func ParseLimitQueryParam(c *gin.Context) (int, error) {
	limit, err := strconv.Atoi(c.DefaultQuery("limit", "10"))
	if err != nil {
		return 0, err
	}
	if limit < 0 {
		return 0, errors.New("page_size must be an positive integer")
	}
	if limit == 0 {
		return 20, nil
	}
	return limit, nil
}

//ParsePageSizeQueryParam ...
func ParsePageSizeQueryParam(c *gin.Context) (int, error) {
	pageSize, err := strconv.Atoi(c.DefaultQuery("page_size", "10"))
	if err != nil {
		return 0, err
	}
	if pageSize < 0 {
		return 0, errors.New("page_size must be an positive integer")
	}
	return pageSize, nil
}

func handleGRPCErr(c *gin.Context, l logger.Logger, err error) bool {
	if err == nil {
		return false
	}
	st, ok := status.FromError(err)
	var errI interface{} = models.InternalServerError{
		Status:  ErrorCodeInternal,
		Message: "Internal Server Error",
	}
	httpCode := http.StatusInternalServerError
	if ok && st.Code() == codes.InvalidArgument {
		httpCode = http.StatusBadRequest
		errI = ErrorBadRequest
	}
	c.JSON(httpCode, models.ResponseError{
		Error: errI,
	})
	if ok {
		l.Error(fmt.Sprintf("code=%d message=%s", st.Code(), st.Message()), logger.Error(err))
	}
	return true
}

func handleGrpcErrWithMessage(c *gin.Context, l logger.Logger, err error, message string, args ...interface{}) bool {
	st, ok := status.FromError(err)
	if !ok || st.Code() == codes.Internal {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeInternal,
				Message: st.Message(),
			},
		})
		l.Error(message, logger.Error(err), logger.Any("req", args))
		return true
	} else if st.Code() == codes.NotFound {
		c.JSON(http.StatusNotFound, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeNotFound,
				Message: st.Message(),
			},
		})
		l.Error(message+", not found", logger.Error(err), logger.Any("req", args))
		return true
	} else if st.Code() == codes.Unavailable {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeInternal,
				Message: "Internal Server Error",
			},
		})
		l.Error(message+", service unavailable", logger.Error(err), logger.Any("req", args))
		return true
	} else if st.Code() == codes.AlreadyExists {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeAlreadyExists,
				Message: st.Message(),
			},
		})
		l.Error(message+", already exists", logger.Error(err), logger.Any("req", args))
		return true
	} else if st.Code() == codes.InvalidArgument {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: st.Message(),
			},
		})
		l.Error(message+", invalid field", logger.Error(err), logger.Any("req", args))
		return true
	} else if st.Code() == codes.DataLoss {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: st.Message(),
			},
		})
		l.Error(message+", invalid field", logger.Error(err), logger.Any("req", args))
		return true
	}

	return false
}

func handleInternalWithMessage(c *gin.Context, l logger.Logger, err error, message string, args ...interface{}) bool {
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeInternal,
				Message: "Internal Server Error",
			},
		})
		l.Error(message, logger.Error(err))
		return true
	}

	return false
}

func handleBadRequestErrWithMessage(c *gin.Context, l logger.Logger, err error, message string, args ...interface{}) bool {
	if err != nil {
		c.JSON(http.StatusBadRequest, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeInvalidJSON,
				Message: "Incorrect data supplied",
			},
		})
		l.Error(message, logger.Error(err), logger.Any("req", args))
		return true
	}
	return false
}

//GetClaims ...
func GetClaims(h *HandlerV1, c *gin.Context) jwtg.MapClaims {
	var (
		ErrUnauthorized = errors.New("unauthorized")
		authorization   models.GetProfileByJwtRequestModel
		claims          jwtg.MapClaims
		err             error
	)

	authorization.Token = c.GetHeader("Authorization")
	if c.Request.Header.Get("Authorization") == "" {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeUnauthorized,
				Message: "Unauthorized request",
			},
		})
		h.log.Error("Unauthorized request: ", logger.Error(ErrUnauthorized))
		return nil
	}

	h.jwtHandler.Token = authorization.Token
	claims, err = h.jwtHandler.ExtractClaims()
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeUnauthorized,
				Message: "Unauthorized request",
			},
		})
		h.log.Error("Unauthorized request: ", logger.Error(err))
		return nil
	}
	return claims
}

func genContent(notifType, objectType, name string) string {

	if config.FollowType == notifType {
		return "followed you"
	} else if config.LikeType == notifType {
		return fmt.Sprintf("thinks your %s is 🔥", strings.ReplaceAll(objectType, "_", " "))
	} else if config.UnfollowType == notifType {
		return "unfollowed you"
	}

	return ""
}
