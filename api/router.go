package api

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	_ "gitlab.com/quizlab/api_gateway/api/docs" // swag
	v1 "gitlab.com/quizlab/api_gateway/api/handlers/v1"
	"gitlab.com/quizlab/api_gateway/api/middleware"
	"gitlab.com/quizlab/api_gateway/api/token"
	"gitlab.com/quizlab/api_gateway/config"
	grpcclient "gitlab.com/quizlab/api_gateway/grpc_client"
	"gitlab.com/quizlab/api_gateway/pkg/logger"
	"gitlab.com/quizlab/api_gateway/storage/repo"
)

// Config ...
type Config struct {
	InMemoryStorage repo.InMemoryStorageI
	Logger          logger.Logger
	GrpcClient      grpcclient.I
	Cfg             config.Config
	CasbinEnforcer  *casbin.Enforcer
}

// New is a constructor for gin.Engine
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(cfg Config) *gin.Engine {
	r := gin.New()

	r.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowHeaders:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowCredentials: true,
	}))
	r.Use(gin.Logger())

	jwtHandler := token.JWTHandler{
		SigninKey: cfg.Cfg.SigninKey,
		Log:       cfg.Logger,
	}

	r.Use(gin.Recovery())
	r.Use(middleware.NewAuthorizer(cfg.CasbinEnforcer, jwtHandler, cfg.Cfg))

	// _ should be replaced with handlerV1
	HandlerV1 := v1.New(cfg.InMemoryStorage, cfg.Logger, cfg.GrpcClient, cfg.Cfg, jwtHandler)

	//Register
	r.POST("/v1/register/", HandlerV1.Register)
	r.GET("/v1/verify/:email/:code/:type/", HandlerV1.Verify)

	// Login
	r.POST("/v1/login/", HandlerV1.Login)
	r.PUT("/v1/logout/", HandlerV1.Logout)
	r.POST("/v1/get_tokens/", HandlerV1.GetNewTokens)
	r.POST("/v1/send-verification/", HandlerV1.SendVerification)
	r.POST("/v1/set-password/", HandlerV1.SetPassword)

	//Score
	r.GET("/v1/total-score/", HandlerV1.GetTotalScore)
	r.PUT("/v1/set-total-score/", HandlerV1.SetTotalScore)

	//Book
	r.GET("/v1/book/", HandlerV1.GetAllBooks)
	r.GET("/v1/book-soon/", HandlerV1.GetAllSoonBooks)
	r.GET("/v1/book/:genre_id/", HandlerV1.GetBookByGenre)
	r.GET("/v1/genres/", HandlerV1.GetGenres)
	r.GET("/v1/questions/:book_id/", HandlerV1.ListQuestionsByBook)

	//Profile
	r.GET("/v1/users/:user_id/profile/", HandlerV1.GetProfile)

	//Leaders
	r.GET("/v1/leaders/", HandlerV1.GetLeadersList)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r
}
