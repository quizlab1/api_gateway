package main

import (
	"fmt"

	"github.com/casbin/casbin/v2"
	defaultrolemanager "github.com/casbin/casbin/v2/rbac/default-role-manager"
	"github.com/casbin/casbin/v2/util"
	gormadapter "github.com/casbin/gorm-adapter/v2"
	"github.com/gomodule/redigo/redis"
	_ "github.com/lib/pq"

	"gitlab.com/quizlab/api_gateway/api"
	"gitlab.com/quizlab/api_gateway/config"
	grpcclient "gitlab.com/quizlab/api_gateway/grpc_client"
	"gitlab.com/quizlab/api_gateway/pkg/logger"
	rds "gitlab.com/quizlab/api_gateway/storage/redis"
	"gitlab.com/quizlab/api_gateway/storage/repo"
)

var (
	log            logger.Logger
	cfg            config.Config
	inMemStrg      repo.InMemoryStorageI
	grpcClient     grpcclient.I
	casbinEnforcer *casbin.Enforcer
)

func initDeps() {
	cfg = config.Load()
	log = logger.New(cfg.LogLevel, "api_gateway")

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	psqlString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresDatabase,
	)

	a, err := gormadapter.NewAdapter("postgres", psqlString, true)
	if err != nil {
		log.Error("new adapter error", logger.Error(err))
		return
	}

	casbinEnforcer, err = casbin.NewEnforcer(cfg.CasbinConfigPath, a)
	if err != nil {
		log.Error("new enforcer error", logger.Error(err))
		return
	}

	err = casbinEnforcer.LoadPolicy()
	if err != nil {
		log.Error("casbin load policy error", logger.Error(err))
		return
	}

	pool := redis.Pool{
		// Maximum number of idle connections in the pool.
		MaxIdle: 80,
		// max number of connections
		MaxActive: 12000,
		// Dial is an application supplied function for creating and
		// configuring a connection.
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", fmt.Sprintf("%s:%d", cfg.RedisHost, cfg.RedisPort))
			if err != nil {
				panic(err.Error())
			}
			return c, err
		},
	}

	inMemStrg = rds.NewRedisRepo(&pool)

	grpcClient, err = grpcclient.New(cfg)
	if err != nil {
		log.Error("grpc dial error", logger.Error(err))
	}
}

func main() {
	initDeps()

	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc("keyMatch", util.KeyMatch)
	casbinEnforcer.GetRoleManager().(*defaultrolemanager.RoleManager).AddMatchingFunc("keyMatch3", util.KeyMatch3)

	server := api.New(api.Config{
		InMemoryStorage: inMemStrg,
		CasbinEnforcer:  casbinEnforcer,
		Cfg:             cfg,
		GrpcClient:      grpcClient,
		Logger:          log,
	})

	if err := server.Run(cfg.HTTPPort); err != nil {
		log.Fatal("error while running gin server", logger.Error(err))
	}
}
