package grpcclient

import (
	"fmt"

	"google.golang.org/grpc"

	"gitlab.com/quizlab/api_gateway/config"
	bookpb "gitlab.com/quizlab/api_gateway/genproto/book_service"
	notifpb "gitlab.com/quizlab/api_gateway/genproto/notification_service"
	userpb "gitlab.com/quizlab/api_gateway/genproto/user_service"
)

// I ...
type I interface {
	UserService() userpb.UserServiceClient
	NotificationService() notifpb.NotificationServiceClient
	BookService() bookpb.BookServiceClient
}

// GrpcClient ...
type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

// New ...
func New(cfg config.Config) (I, error) {

	connUser, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.UserServiceHost, cfg.UserServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("user service dial host: %s port:%d err: %s",
			cfg.UserServiceHost, cfg.UserServicePort, err)
	}

	connNotif, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.NotifServiceHost, cfg.NotifServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("notification service dial host: %s port:%d err: %s",
			cfg.NotifServiceHost, cfg.NotifServicePort, err)
	}

	connBook, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.BookServiceHost, cfg.BookServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("book service dial host: %s port:%d err: %s",
			cfg.BookServiceHost, cfg.BookServicePort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"user_service":         userpb.NewUserServiceClient(connUser),
			"notification_service": notifpb.NewNotificationServiceClient(connNotif),
			"book_service": bookpb.NewBookServiceClient(connBook),
		},
	}, nil
}

// UserService func
func (g *GrpcClient) UserService() userpb.UserServiceClient {
	return g.connections["user_service"].(userpb.UserServiceClient)
}

// NotificationService func
func (g *GrpcClient) NotificationService() notifpb.NotificationServiceClient {
	return g.connections["notification_service"].(notifpb.NotificationServiceClient)
}

// BookService func
func (g *GrpcClient) BookService() bookpb.BookServiceClient {
	return g.connections["book_service"].(bookpb.BookServiceClient)
}
