# workspace (GOPATH) configured at /go
FROM golang:1.14 as builder


#
RUN mkdir -p $GOPATH/src/gitlab.com/quizlab/api_gateway
WORKDIR $GOPATH/src/gitlab.com/quizlab/api_gateway

# Copy the local package files to the container's workspace.
COPY . ./

# installing depends and build
RUN export CGO_ENABLED=0 && \
    export GOOS=linux && \
    make build && \
    mv ./bin/api_gateway /



FROM alpine
COPY --from=builder api_gateway .
RUN mkdir html
RUN mkdir config
COPY ./config/rbac_model.conf ./config/rbac_model.conf
COPY ./html/resetpass.html ./html/resetpass.html
COPY ./html/email.html ./html/email.html
ENTRYPOINT ["/api_gateway"]