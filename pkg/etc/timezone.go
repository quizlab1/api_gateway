package etc

import (
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

var timezones map[string]string = map[string]string{
	"ACDT":  "UTC+10:30", // Australian Central Daylight Savings Time
	"ACST":  "UTC+09:30", // Australian Central Standard Time
	"ACT":   "UTC−05",    // Acre Time
	"ADT":   "UTC−03",    // Atlantic Daylight Time
	"AEDT":  "UTC+11",    // Australian Eastern Daylight Savings Time
	"AEST":  "UTC+10",    // Australian Eastern Standard Time
	"AFT":   "UTC+04:30", // Afghanistan Time
	"AKDT":  "UTC−08",    // Alaska Daylight Time
	"AKST":  "UTC−09",    // Alaska Standard Time
	"ALMT":  "UTC+06",    // Alma-Ata Time
	"AMT":   "UTC−04",    // Amazon Time (Brazil)
	"AMST":  "UTC−03",    // Amazon Summer Time (Brazil)
	"ANAT":  "UTC+12",    // Anadyr Time
	"AQTT":  "UTC+05",    // Aqtobe Time
	"ART":   "UTC−03",    // Argentina Time
	"AST":   "UTC−04",    // Atlantic Standard Time
	"AWST":  "UTC+08",    // Australian Western Standard Time
	"AZOST": "UTC±00",    // Azores Summer Time
	"AZOT":  "UTC−01",    // Azores Standard Time
	"GMT":   "UTC±00",    // Greenwich Mean Time
	"EDT":   "UTC−04",    // Eastern Daylight Time
}

// ExtractTimezoneDiff extracts seconds relative to the UTC±00 timezone
func ExtractTimezoneDiff(c *gin.Context) (time.Duration, error) {
	var (
		utc      int    = 0
		timezone string = "0"
		err      error
	)

	if c.GetHeader("Timezone") != "" {
		timezone = c.GetHeader("Timezone")
	}

	current, err := strconv.Atoi(timezone)
	if err != nil {
		return 0, err
	}

	diff := current - utc

	return time.Duration(diff) * time.Second, err
}

// ConvertUTCToLocal converts UTC±00 time to the local time of a user
func ConvertUTCToLocal(datetime string, layout string, c *gin.Context) (time.Time, error) {
	diff, err := ExtractTimezoneDiff(c)
	if err != nil {
		return time.Time{}, err
	}

	t, err := time.Parse(layout, datetime)
	if err != nil {
		return time.Time{}, err
	}

	t = t.Add(diff)

	return t, nil
}

// ConvertLocalToUTC converts local time of a user to the UTC±00
func ConvertLocalToUTC(datetime string, layout string, c *gin.Context) (time.Time, error) {
	diff, err := ExtractTimezoneDiff(c)
	if err != nil {
		return time.Time{}, err
	}

	t, err := time.Parse(layout, datetime)
	if err != nil {
		return time.Time{}, err
	}

	t = t.Add(-diff)

	return t, nil
}
