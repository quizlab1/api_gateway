package etc

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var runes = []rune("abcdefghijklmnopqrstuvwxyz123456789")

// RandStringRunes generates random string of a specified length
func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = runes[rand.Intn(len(runes))]
	}

	return string(b)
}
