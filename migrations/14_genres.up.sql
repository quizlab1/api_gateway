DO $$
BEGIN
    DECLARE 
        -- write needed roles inside {} splitting each role from others by comma (,)
        roles VARCHAR(50)[] := '{authorized}'::VARCHAR(50)[];
        r VARCHAR(50); -- individual role, value of which assigned as a result of a loop iteration
        url TEXT := '/v1/genres/'; -- url endpoint
        method VARCHAR(10) := 'GET'; -- commonly used methods: GET, POST, PUT, PATCH, DELETE
    BEGIN
        FOREACH r IN ARRAY roles
        LOOP
            INSERT INTO casbin_rule(p_type, v0, v1, v2) VALUES ('p', r, url, method) 
                ON CONFLICT DO NOTHING;
        END LOOP;
    END;
END $$;